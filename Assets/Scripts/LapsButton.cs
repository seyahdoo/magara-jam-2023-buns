using System;
using LapsRuntime;
using UnityEngine;

public class LapsButton : LapsComponent
{
    public override void GetOutputSlots(SlotList slots) {
        slots.Add(new LogicSlot("On Pressed", 0));
        slots.Add(new LogicSlot("On Released", 1));
    }

    private void OnTriggerEnter2D(Collider2D other) {
        FireOutput(0);
    }

    private void OnTriggerExit2D(Collider2D other) {
        FireOutput(1);
    }
}
