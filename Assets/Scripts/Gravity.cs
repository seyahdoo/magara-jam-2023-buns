using UnityEngine;

public class Gravity : MonoBehaviour {
    public float gravityChangeSpeed = 5f;
    public float gravityAmount = 10f;

    private static Vector2 _targetGravity = Vector2.down;
    private Vector2 currentGravity = Vector2.down;

    public static void SetGravity(Vector2 target) {
        _targetGravity = target;
    }

    private void FixedUpdate() {
        currentGravity = Vector2.MoveTowards(currentGravity, _targetGravity, Time.fixedDeltaTime * gravityChangeSpeed);
        Physics2D.gravity = currentGravity * gravityAmount;
    }
}
